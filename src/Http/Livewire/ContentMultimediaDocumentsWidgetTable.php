<?php

namespace Bittacora\ContentMultimediaDocuments\Http\Livewire;

use Bittacora\ContentMultimediaDocuments\Models\ContentMultimediaDocumentsModel;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class ContentMultimediaDocumentsWidgetTable extends DataTableComponent
{

    public int $contentId;
    public bool $reordering = true;
    public string $reorderingMethod = 'reorder';
    public bool $showPerPage = false;
    public bool $showPagination = false;
    public bool $showSearch = false;
    public array $perPageAccepted = [0];

    protected $listeners = ['refreshContentMultimediaDocumentsWidgetTable' => '$refresh'];

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function columns(): array
    {
        return [
//            Column::make('Formato', 'id')->view('content-multimedia-documents::livewire.datatable.format-column'),
            Column::make('Título', 'id')->view('content-multimedia-documents::livewire.datatable.title-column'),
            Column::make('Nombre del archivo', 'id')->view('content-multimedia-documents::livewire.datatable.filename-column'),
            Column::make('Activo', 'active')->view('content-multimedia-documents::livewire.datatable.active-column'),
            Column::make('Destacado', 'featured')->view('content-multimedia-documents::livewire.datatable.featured-column'),
            Column::make('Orden', 'order_column')->view('content-multimedia-documents::livewire.datatable.order-column'),
            Column::make('', 'id')->view('content-multimedia-documents::livewire.datatable.media-detach')
        ];
    }

    public function builder(): Builder
    {
        return ContentMultimediaDocumentsModel::query()->select(['id', 'content_id', 'multimedia_id', 'order_column', 'active', 'featured'])->where('content_id', $this->contentId)->orderBy('order_column', 'ASC');
    }

    public function reorder($list){
        foreach($list as $item){
            ContentMultimediaDocumentsModel::where('id', $item['value'])->update(['order_column' => $item['order']]);
        }
    }
}
