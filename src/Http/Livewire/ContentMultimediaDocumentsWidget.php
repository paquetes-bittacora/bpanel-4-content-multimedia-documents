<?php

namespace Bittacora\ContentMultimediaDocuments\Http\Livewire;

use Bittacora\ContentMultimediaDocuments\ContentMultimediaDocuments;
use Bittacora\Multimedia\Models\Multimedia;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;
use Bittacora\ContentMultimediaDocuments\Models\ContentMultimediaDocumentsModel;

class ContentMultimediaDocumentsWidget extends Component
{
    public int $contentId;
    public ?Collection $documents = null;

    protected $listeners = ['refreshWidget' => '$refresh'];

    public function mount()
    {
        $contentMultimediaData = ContentMultimediaDocumentsModel::where([
            ['content_id', '=', $this->contentId]
        ])->orderBy('order_column', 'ASC')->get();

        if (!empty($contentMultimediaData)){
            foreach ($contentMultimediaData as $key => $content) {
                /**
                 * @var ContentMultimediaDocumentsModel $content
                 */
                $multimedia = Multimedia::where('id', $content->multimedia_id)->with('mediaModel')->first();
                $content->setAttribute('multimedia', $multimedia);

            }

            $this->documents = $contentMultimediaData;
        }
    }

    public function render()
    {
        return view('content-multimedia-documents::livewire.content-multimedia-documents-widget')->with([
            'documents' => $this->documents,
            'contentId' => $this->contentId
        ]);
    }
}
