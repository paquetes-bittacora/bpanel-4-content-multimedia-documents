<?php

namespace Bittacora\ContentMultimediaDocuments\Commands;

use Illuminate\Console\Command;

class ContentMultimediaDocumentsCommand extends Command
{
    public $signature = 'content-multimedia-documents';

    public $description = 'My command';

    public function handle()
    {
        $this->comment('All done');
    }
}
