<?php

namespace Bittacora\ContentMultimediaDocuments;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\ContentMultimediaDocuments\ContentMultimediaDocuments
 */
class ContentMultimediaDocumentsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'content-multimedia-documents';
    }
}
