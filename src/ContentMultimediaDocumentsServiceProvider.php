<?php

namespace Bittacora\ContentMultimediaDocuments;

use Bittacora\ContentMultimediaDocuments\Http\Livewire\ContentMultimediaDocumentsWidget;
use Bittacora\ContentMultimediaDocuments\Http\Livewire\ContentMultimediaDocumentsWidgetTable;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\ContentMultimediaDocuments\Commands\ContentMultimediaDocumentsCommand;

class ContentMultimediaDocumentsServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('content-multimedia-documents')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_content-multimedia-documents_table')
            ->hasCommand(ContentMultimediaDocumentsCommand::class);
    }

    public function register()
    {
        $this->app->bind('content-multimedia-documents', function($app){
            return new ContentMultimediaDocuments();
        });
    }

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/', 'content-multimedia-documents');
        Livewire::component('content-multimedia-documents::content-multimedia-documents-widget', ContentMultimediaDocumentsWidget::class);
        Livewire::component('content-multimedia-documents::content-multimedia-documents-widget-table', ContentMultimediaDocumentsWidgetTable::class);
    }
}
