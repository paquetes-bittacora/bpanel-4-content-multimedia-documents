<x-livewire-tables::bs4.table.cell>
    <div class="file-format-icon text-center">
        <img src="{{asset("assets_bpanel/icons/".\Bittacora\Utils\UtilsFacade::extractExtensionFromFilename($row->multimedia->mediaModel->file_name) .".png")}}" alt="">
    </div>
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    @livewire('utils::model-translatable-attribute', ['model' => $row->multimedia, 'attribute' => 'title'], key('title-documents-'.$row->id))
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    {{$row->multimedia->mediaModel->file_name}}
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-documents-'.$row->id))
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    @livewire('utils::datatable-default', ['fieldName' => 'featured', 'model' => $row, 'value' => $row->featured, 'size' => 'xxs'], key('featured-documents-'.$row->id))
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    <div class="d-flex justify-content-center align-items-center">
        <span class="align-items-center badge bgc-purple-d1 pos-rel text-white radius-4 px-3">
            <span class="bgc-primary-tp4 opacity-5 position-tl h-100 w-100 radius-4"></span>
            <span class="pos-rel">
                {{$row->order_column}}
            </span>
        </span>
    </div>
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    @livewire('content-multimedia::detach-multimedia-button', ['contentMultimediaId' => $row->id, 'contentId' => $row->content_id, 'type' => 'D'], key('detach-documents-'.$row->multimedia_id.$row->content_id))
</x-livewire-tables::bs4.table.cell>
