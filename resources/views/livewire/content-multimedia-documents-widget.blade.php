<div class="widget-box widget-color-blue ui-sortable-handle mb-4" id="widget-box-7">
    <div class="widget-header widget-header-small">
        <h6 class="widget-title smaller">Documentos adjuntos</h6>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            @livewire('content-multimedia-documents::content-multimedia-documents-widget-table', ['contentId' => $contentId], key('documents-'.$contentId))
            @livewire('multimedia::media-library-images', ['contentId' => $contentId, 'type' => 'documents'], key('media-library-documents-'.$contentId))
        </div>
    </div>
</div>



@push('scripts')
    @vite('vendor/bittacora/bpanel4-panel/resources/assets/js/livewire-sortable.js')
@endpush
